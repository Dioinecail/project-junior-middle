﻿namespace Custom.Inputs
{
    using System;
    using UnityEngine;

    public class InputController : MonoBehaviour
    {
        public static event Action<Vector2> onMovemenetEvent;
        public static event Action<float> onZoomEvent;

        public TouchInputHandler touchInputs;
        public MouseInputHandler mouseInputs;



        private bool IsPhone()
        {
            return Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer;
        }

        private void OnEnable()
        {
            if (IsPhone())
            {
                touchInputs.onMove += onMovemenetEvent;
                touchInputs.onZoom += onZoomEvent;
            }
            else
            {
                mouseInputs.onMove += onMovemenetEvent;
                mouseInputs.onZoom += onZoomEvent;
            }
        }

        private void OnDisable()
        {
            if (IsPhone())
            {
                touchInputs.onMove -= onMovemenetEvent;
                touchInputs.onZoom -= onZoomEvent;
            }
            else
            {
                mouseInputs.onMove -= onMovemenetEvent;
                mouseInputs.onZoom -= onZoomEvent;
            }
        }
    }
}