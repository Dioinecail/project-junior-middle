﻿namespace Custom.Inputs
{
    using System;
    using UnityEngine;

    public class MouseInputHandler : MonoBehaviour, InputHandler
    {
        public event Action<Vector2> onMove;
        public event Action<float> onZoom;

        public float moveInputThreshold;

        private Vector2 startMousePosition;
        private bool isHandlingMovementInputs;



        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
                BeginHandlingMouseInputs();

            if (Input.GetMouseButton(0))
                HandleMouseInputs();

            if (Input.GetMouseButtonUp(0))
                EndHandlingMouseInputs();

            HandleMouseScrollInputs();
        }

        private void BeginHandlingMouseInputs()
        {
            startMousePosition = Input.mousePosition;
            isHandlingMovementInputs = true;
        }

        private void HandleMouseInputs()
        {
            if (!isHandlingMovementInputs)
                return;

            Vector2 currentMousePosition = Input.mousePosition;

            if ((currentMousePosition - startMousePosition).magnitude > moveInputThreshold)
            {
                Vector2 movementDirection = GetDirection(startMousePosition, currentMousePosition);

                onMove?.Invoke(movementDirection);
                isHandlingMovementInputs = false;
            }
        }

        private void EndHandlingMouseInputs()
        {
            isHandlingMovementInputs = false;
        }

        private Vector2 GetDirection(Vector2 start, Vector2 end)
        {
            Vector2 direction = end - start;

            if(Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
            {
                // horizontal
                direction.y = 0;
                direction.x = Mathf.Sign(direction.x);
            }
            else
            {
                // vertical
                direction.x = 0;
                direction.y = Mathf.Sign(direction.y);
            }

            return direction;
        }

        private void HandleMouseScrollInputs()
        {
            Vector2 scrollDelta = Input.mouseScrollDelta;

            onZoom.Invoke(scrollDelta.y);
        }
    }
}