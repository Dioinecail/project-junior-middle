﻿namespace Custom.Inputs
{
    using System;
    using UnityEngine;

    public class TouchInputHandler : MonoBehaviour, InputHandler
    {
        public event Action<Vector2> onMove;
        public event Action<float> onZoom;

        public float moveInputThreshold;
        public float pinchDistanceThreshold;

        private Vector2 startMousePosition;
        private bool isHandlingMovementInputs;
        private float lastPinchDistance;



        private void Update()
        {
            if(Input.touchCount == 1)
            {
                // handle movement inputs
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                    BeginHandlingInputs(touch.position);

                if (touch.phase == TouchPhase.Moved)
                    HandleInputs(touch.position);

                if (touch.phase == TouchPhase.Ended)
                    EndHandlingInputs();
            }
            else if (Input.touchCount == 2)
            {
                // handle zoom inputs
                Touch touch1 = Input.GetTouch(0);
                Touch touch2 = Input.GetTouch(1);

                if (touch1.phase == TouchPhase.Began || touch2.phase == TouchPhase.Began)
                    BeginHandlingPinchInputs(touch1.position, touch2.position);
                // begin

                HandlePinchInputs(touch1.position, touch2.position);
            }
        }

        private void BeginHandlingInputs(Vector2 position)
        {
            startMousePosition = position;
            isHandlingMovementInputs = true;
        }

        private void HandleInputs(Vector2 position)
        {
            if (!isHandlingMovementInputs)
                return;

            Vector2 currentMousePosition = position;

            if ((currentMousePosition - startMousePosition).magnitude > moveInputThreshold)
            {
                Vector2 movementDirection = GetDirection(startMousePosition, currentMousePosition);

                onMove?.Invoke(movementDirection);
                isHandlingMovementInputs = false;
            }
        }

        private void EndHandlingInputs()
        {
            isHandlingMovementInputs = false;
        }

        private Vector2 GetDirection(Vector2 start, Vector2 end)
        {
            Vector2 direction = end - start;

            if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
            {
                // horizontal
                direction.y = 0;
                direction.x = Mathf.Sign(direction.x);
            }
            else
            {
                // vertical
                direction.x = 0;
                direction.y = Mathf.Sign(direction.y);
            }

            return direction;
        }

        private void BeginHandlingPinchInputs(Vector2 finger1Position, Vector2 finger2Position)
        {
            lastPinchDistance = (finger1Position - finger2Position).magnitude;
        }

        private void HandlePinchInputs(Vector2 finger1Position, Vector2 finger2Position)
        {
            float currentPinchDistance = (finger1Position - finger2Position).magnitude;

            float pinchDeltaDistance = currentPinchDistance - lastPinchDistance;

            if (Mathf.Abs(pinchDeltaDistance) > pinchDistanceThreshold)
            {
                onZoom?.Invoke(pinchDeltaDistance);
                BeginHandlingPinchInputs(finger1Position, finger2Position);
            }
        }
    }
}