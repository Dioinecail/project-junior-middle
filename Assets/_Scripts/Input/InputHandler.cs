﻿namespace Custom.Inputs
{
    using System;
    using UnityEngine;

    public interface InputHandler
    {
        event Action<Vector2> onMove;
        event Action<float> onZoom;
    }
}