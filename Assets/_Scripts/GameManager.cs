﻿namespace Custom.Managers
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using Custom.Utility;
    using UnityEngine;

    public class GameManager : MonoBehaviour
    {
        private const int MIN_SIZE = 5;
        private const int MAX_SIZE = 10001;

        [Range(MIN_SIZE, MAX_SIZE)]
        public int size;
        [Range(0, 1f)]
        public float fillPercent;
        public int seed;

        [Header("DEBUG")]
        [Range(MIN_SIZE, MAX_SIZE)]
        public int currentRange;
        [Range(MIN_SIZE, MAX_SIZE)]
        public int xCoord, yCoord;
        public int amount;

        private Tile[,] worldTiles;



        private void Start()
        {
            CreateWorld();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                GetSortedPlanets();
            }
        }

        private void CreateWorld()
        {
            worldTiles = WorldGeneration.GenerateWorld(size, size, fillPercent, seed);
        }

        private void GetSortedPlanets()
        {
            Tile[] sortedPlanets = RankSorter.GetHighestClosestRanks(ref worldTiles, xCoord, yCoord, currentRange, amount);

            StringBuilder debugOutput = new StringBuilder();

            debugOutput.AppendLine("Sorted planets:");

            for (int i = 0; i < sortedPlanets.Length; i++)
            {
                debugOutput.AppendLine($"Planet-[{i + 1}], coords:[{sortedPlanets[i].xCoord},{sortedPlanets[i].yCoord}], rank:{sortedPlanets[i].rank}");
            }

            Debug.Log(debugOutput.ToString());
        }

        private void OnValidate()
        {
            xCoord = Mathf.Clamp(xCoord, 0, size);
            yCoord = Mathf.Clamp(yCoord, 0, size);
            currentRange = Mathf.Clamp(currentRange, MIN_SIZE, size);
        }
    }
}