﻿using System;
using System.Collections.Generic;

namespace Custom.Utility
{
    public static class RankSorter
    {
        public static Tile[] GetHighestClosestRanks(ref Tile[,] world, int xCoord, int yCoord, int range, int amount)
        {
            // return <amount> of closest highest ranked planets

            List<Tile> sortingList = new List<Tile>();

            for (int x = xCoord - range; x < xCoord + range; x++)
            {
                for (int y = yCoord - range; y < yCoord + range; y++)
                {
                    if (!IsWithinRange(x, y, ref world))
                        continue;

                    if (world[x, y].isPlanet)
                        sortingList.Add(world[x, y]);
                }
            }

            Tile[] sortedArray = sortingList.ToArray();
            sortingList.Clear();

            QuickSort(ref sortedArray, 0, sortedArray.Length - 1);

            List<Tile> trimmedList = new List<Tile>();

            for (int i = 0; i < amount; i++)
            {
                if (i >= sortedArray.Length)
                    continue;

                trimmedList.Add(sortedArray[sortedArray.Length - i - 1]);
            }

            return trimmedList.ToArray();
        }

        private static bool IsWithinRange(int x, int y, ref Tile[, ] world)
        {
            return x >= 0 && x < world.GetLength(0) && y >= 0 && y < world.GetLength(1);
        }

        private static void QuickSort(ref Tile[] arr, int left, int right)
        {
            if (left < right)
            {
                int pivot = Partition(ref arr, left, right);

                if (pivot > 1)
                {
                    QuickSort(ref arr, left, pivot - 1);
                }

                if (pivot + 1 < right)
                {
                    QuickSort(ref arr, pivot + 1, right);
                }
            }
        }

        private static int Partition(ref Tile[] arr, int left, int right)
        {
            Tile pivot = arr[left];

            while (true)
            {
                while (arr[left].CompareTo(pivot) < 0)
                {
                    left++;
                }

                while (arr[right].CompareTo(pivot) > 0)
                {
                    right--;
                }

                if (left < right)
                {
                    if (arr[left].CompareTo(arr[right]) == 0)
                        return right;

                    Tile temp = arr[left];
                    arr[left] = arr[right];
                    arr[right] = temp;
                }
                else
                {
                    return right;
                }
            }
        }
    }
}