﻿namespace Custom.Utility
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

    public struct Tile : IComparable<Tile>
	{
		public int xCoord;
		public int yCoord;
		public int rank;
        public bool isPlanet;

        public Tile(int _x, int _y, int _rank, bool _isPlanet)
        {
            xCoord = _x;
            yCoord = _y;
            rank = _rank;
            isPlanet = _isPlanet;
        }

        public int CompareTo(Tile other)
        {
            return this.rank - other.rank;
        }
    }

	public static class WorldGeneration
	{
        public static Tile[,] GenerateWorld(int sizeX, int sizeY, float fillPercent, int seed)
		{
            System.Random rnd = new System.Random(seed);

            Tile[,] world = new Tile[sizeX, sizeY];

            for (int x = 0; x < world.GetLength(0); x++)
            {
                for (int y = 0; y < world.GetLength(1); y++)
                {
                    bool isPlanet = (rnd.Next(0, 100) / 100f) < fillPercent;
                    int rank = rnd.Next(0, 10000);

                    world[x, y] = new Tile(x, y, rank, isPlanet);
                }
            }

            return world;
		}
	}
}